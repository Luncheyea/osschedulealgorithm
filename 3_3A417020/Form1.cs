﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3_3A417020
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private struct myTask
        {
            internal String Name;
            internal int ExecutionTime;
            internal int ArrivalTime;
            internal int Priority;

            internal int waitTime1;
            internal int waitTime2;
            internal int runTime;

            public myTask(string _name, int _execution, int _arrival, int _priority)
            {
                Name = _name;
                ExecutionTime = _execution;
                ArrivalTime = _arrival;
                Priority = _priority;

                waitTime1 = 0;
                waitTime2 = 0;
                runTime = 0;
            }

            public bool isEquals(myTask obj)
            {
                return (this.Name == (obj).Name) && (this.ExecutionTime == (obj).ExecutionTime);
            }

            public override string ToString()
            {
                // return base.ToString();
                // return String.Format("{0}({1}, {2}, {3}, {4})", Name, ExecutionTime, ArrivalTime, SplitTime, Priority);
                return String.Format("{0}({1, 2})", Name, ExecutionTime);
            }
        }

        private List<myTask> taskList = new List<myTask>();
        List<List<List<myTask>>> tasksRinningList = new List<List<List<myTask>>>();

        private void Form1_Load(object sender, EventArgs e)
        {
            taskList.Add(new myTask("A", 10, 0, 1));
            taskList.Add(new myTask("B", 12, 1, 3));
            taskList.Add(new myTask("C", 2, 13, 3));
            taskList.Add(new myTask("D", 8, 7, 0));
            taskList.Add(new myTask("E", 4, 6, 3));
            //
            cmbPriority.SelectedIndex = 2;
            // /////////////
            ptbcTable.Width = 600;
            ptbcTable.Height = 250;//80;
            showTaskTable(ptbcTable, ptbcTable.Width, ptbcTable.Height);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tbName.Text == "")
            {
                MessageBox.Show("請輸入行程名稱！");
                return;
            }

            myTask task = new myTask();
            task.Name = tbName.Text;
            task.ExecutionTime = (int)numExecutionTime.Value;
            task.ArrivalTime = (int)numArrivalTime.Value;
            task.Priority = cmbPriority.SelectedIndex;

            taskList.Add(task);

            const int tableBaseHeight = 80, tableCellHeight = 30;
            ptbcTable.Height = tableBaseHeight + tableCellHeight * taskList.Count;
            showTaskTable(ptbcTable, ptbcTable.Width, ptbcTable.Height);

            tbName.Clear();
            tbName.Focus();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            taskList.Clear();
            tasksRinningList.Clear();

            lbResult.Text = "";

            ptbcTable.Image = null;
            ptbcTable.Height = 100;

            ptbRunningLine.Image = null;
            ptbRunningLine.Height = 100;
            ptbRunningLine.Width = 100;

            GC.Collect();

            ptbcTable.Width = 600;
            ptbcTable.Height = 80;
            showTaskTable(ptbcTable, ptbcTable.Width, ptbcTable.Height);
        }

        private void cmbTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (taskList.Count == 0)
            {
                MessageBox.Show("沒有任何行程！");
                return;
            }

            Double optimal = 0.0, average = 0.0;
            String name = "先來先服務";

            optimal = FirstComeFirstServe();
            //if (ShortJobFirst() < optimal)
            //{
            //    optimal = ShortJobFirst();
            //    name = "最短工作先服務";
            //}
            if (ShortJobFirst_Preemptive() < optimal)
            {
                optimal = ShortJobFirst_Preemptive();
                name = "最短工作先服務";
            }
            if (ShortestRemainingTime() < optimal)
            {
                optimal = ShortestRemainingTime();
                name = "剩餘最短的工作先服務";
            }
            if (HighPriorityFrist() < optimal)
            {
                optimal = HighPriorityFrist();
                name = "優先權";
            }
            if (RoundRobin() < optimal)
            {
                optimal = RoundRobin();
                name = "巡迴服務";
            }
            if (MultipleLevelFeedbackQueue() < optimal)
            {
                optimal = MultipleLevelFeedbackQueue();
                name = "多層回饋佇列";
            }


            tasksRinningList.Clear();
            switch (((ComboBox)sender).SelectedIndex)
            {
                case 0:
                    average = FirstComeFirstServe();
                    break;
                case 1:
                    //average = ShortJobFirst();
                    average = ShortJobFirst_Preemptive();
                    break;
                case 2:
                    average = ShortestRemainingTime();
                    break;
                case 3:
                    average = HighPriorityFrist();
                    break;
                case 4:
                    average = RoundRobin(5);
                    break;
                case 5:
                    average = MultipleLevelFeedbackQueue();
                    break;
            }

            lbResult.Text = "平均等待時間：" + average.ToString("F3") + "\n\n定量評估：" + name + "\n　　　　　" + optimal.ToString("F3");

            showTasksRunning(ptbRunningLine, ref tasksRinningList);
        }

        // ////////
        private void showTaskTable(PictureBox pictureBox, int width, int height)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics graphics = Graphics.FromImage(bitmap);
            String[] titleName = new String[] { "行程名稱", "到達時間", "執行時間", "優先順序" };
            Pen pen = new Pen(Color.Black, 1);
            Font font = new Font("Consolas", 14, FontStyle.Regular);
            const int top = 10, left = 10, right = 30;
            const int titleWordWidth = 10, titleWordHeight = 10, wordWidth = 40, wordHeight = 5;
            const int tableWidth = 480, tableHeight = 40;
            const int titleNum = 4;
            int cellHeight = 30, cellWidth = (tableWidth - left - right) / titleNum;

            graphics.Clear(Color.Snow);
            graphics.DrawLine(pen, new Point(left, top), new Point(left + cellWidth * titleNum, top));
            graphics.DrawLine(pen, new Point(left, tableHeight + top), new Point(left + cellWidth * titleNum, tableHeight + top));

            for (int i = 0; i <= titleNum; i++)
                graphics.DrawLine(pen, new Point(left + cellWidth * i, top), new Point(left + cellWidth * i, tableHeight + top));
            for (int i = 0; i < titleNum; i++)
                graphics.DrawString(titleName[i], font, Brushes.Black, new PointF(left + cellWidth * i + titleWordWidth, top + titleWordHeight));
            // /////////////
            int index = 0;
            foreach (myTask _task in this.taskList)
            {
                for (int i = 0; i <= titleNum; i++)
                    graphics.DrawLine(pen, new Point(left + cellWidth * i, top + tableHeight + cellHeight * index), new Point(left + cellWidth * i, top + tableHeight + cellHeight * (1 + index)));
                graphics.DrawLine(pen, new Point(left, top + tableHeight + cellHeight * (1 + index)), new Point(left + cellWidth * titleNum, top + tableHeight + cellHeight * (1 + index)));

                String[] taskArg = new string[] { _task.Name, string.Format("{0, 2}", _task.ArrivalTime), string.Format("{0, 2}", _task.ExecutionTime), string.Format("{0, 2}", _task.Priority) };
                for (int i = 0; i < titleNum; i++)
                    graphics.DrawString(taskArg[i], font, Brushes.Black, new PointF(left + cellWidth * i + wordWidth, top + tableHeight + wordHeight + cellHeight * index));
                index++;
            }

            pictureBox.Image = (Image)bitmap;
        }

        private void showTasksRunning(PictureBox pictureBox, ref List<List<List<myTask>>> tasksRinningList)
        {
            const int top = 30, bottom = 10, left = 20, right = 10;
            const int spiltLine = 20, runningLength = 100;
            const int spiltLineTop = 23, spiltLineBottom = 0, spiltLineLeft = 8;
            const int wordHeight = 21, timeHeight = 70;
            const int baseWidth = 100, baseHeight = 30;

            int runningLineWidth = tasksRinningList[0].Count, totalRunningLineHeight = 0;
            List<int> runningLineHeight = new List<int>();
            int i = 0;
            foreach (List<List<myTask>> _runningList in tasksRinningList)
            {
                foreach (List<myTask> _taskList in _runningList)
                {
                    if (runningLineHeight.Count < i + 1)
                    {
                        runningLineHeight.Add(_taskList.Count);
                    }
                    else if (runningLineHeight[i] < _taskList.Count)
                    {
                        runningLineHeight.Add(_taskList.Count);
                        runningLineHeight.RemoveAt(i);
                    }
                }
                i++;
            }

            foreach (int _runningLineHeight in runningLineHeight)
            {
                totalRunningLineHeight += _runningLineHeight;
            }

            pictureBox.Width = baseWidth + runningLength * runningLineWidth;
            pictureBox.Height = baseHeight + wordHeight * totalRunningLineHeight + timeHeight * tasksRinningList.Count;
            int runningLine = 0;

            Bitmap bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            Pen pen = new Pen(Color.Black, 1);
            Font font = new Font("Consolas", 14, FontStyle.Regular);


            graphics.Clear(Color.AliceBlue);
            i = 0;
            foreach (List<List<myTask>> _runningList in tasksRinningList)
            {
                int runTime = 0;
                foreach (List<myTask> _taskList in _runningList)
                {
                    pen.Width = 2;
                    graphics.DrawLine(pen, new Point(left + runningLength * runTime, top + runningLine), new Point(left + runningLength * runTime, top + spiltLine + runningLine));
                    pen.Width = 1;
                    graphics.DrawLine(pen, new Point(left + runningLength * runTime, top + spiltLine / 2 + runningLine), new Point(left + runningLength * (1 + runTime), top + spiltLine / 2 + runningLine));
                    graphics.DrawString(runTime.ToString(), font, Brushes.Black, new PointF(left - spiltLineLeft + runningLength * runTime, top - spiltLineTop + runningLine));

                    int index = 0;
                    foreach (myTask _task in _taskList)
                    {
                        if (index == 0 && ((_runningList.Count - 1 == runTime || _runningList[runTime + 1].Count == 0 || !_runningList[runTime][0].isEquals(_runningList[runTime + 1][0]) || (runTime > 0 && _runningList[runTime - 1].Count != 0 && _runningList[runTime - 1][0].Name == _runningList[runTime][0].Name && _runningList[runTime - 1][0].ExecutionTime != _runningList[runTime][0].ExecutionTime))))
                            graphics.DrawString(_task.ToString(), font, Brushes.DarkRed, new PointF(left - spiltLineLeft + runningLength * runTime, top + spiltLine + spiltLineBottom + wordHeight * index + runningLine));
                        else
                            graphics.DrawString(_task.ToString(), font, Brushes.Black, new PointF(left - spiltLineLeft + runningLength * runTime, top + spiltLine + spiltLineBottom + wordHeight * index + runningLine));
                        index++;
                    }
                    runTime++;
                }
                runningLine += timeHeight + runningLineHeight[i] * wordHeight;

                i++;
            }

            pictureBox.Image = (Image)bitmap;
        }

        private void bubbleSort(ref List<myTask> tasks, Comparison<myTask> comparison)
        {
            myTask tmp;
            for (int i = 0; i < tasks.Count; i++)
                for (int j = 0; j < tasks.Count - 1 - i; j++)
                    if (comparison(tasks[j], tasks[j + 1]) == 1)
                    {
                        tmp = tasks[j];
                        tasks[j] = tasks[j + 1];
                        tasks[j + 1] = tmp;
                    }
        }

        private Double getAverageWaitTime()
        {
            Double AverageWaitTime = 0.0;
            for (int k = 0; k < taskList.Count; k++)
            {
                AverageWaitTime += taskList[k].waitTime1;

                myTask t = taskList[k];
                t.waitTime1 = 0;
                taskList[k] = t;
            }

            return AverageWaitTime / taskList.Count;
        }

        // //////////
        private Double FirstComeFirstServe()
        {
            List<myTask> runTaskList = new List<myTask>(taskList);
            List<List<myTask>> runningList = new List<List<myTask>>();

            bubbleSort(ref runTaskList, (task1, task2) =>
                task1.ArrivalTime.CompareTo(task2.ArrivalTime));

            int runTime = 0;
            while (runTaskList.Count > 0)
            {
                Boolean isFirst = true;
                List<myTask> tmpTasks = new List<myTask>();
                for (int i = 0; i < runTaskList.Count; i++)
                {
                    if (runTaskList[i].ArrivalTime <= runTime)
                    {
                        tmpTasks.Add(runTaskList[i]);
                        if (runTaskList[i].ExecutionTime == 0)
                        {
                            runTaskList.RemoveAt(i);
                            i--;
                        }
                        else if (isFirst)
                        {
                            myTask t = runTaskList[i];
                            t.ExecutionTime--;
                            runTaskList[i] = t;
                            isFirst = false;


                        }
                        else
                        {
                            for (int j = 0; j < taskList.Count; j++)
                                if (runTaskList[i].Name == taskList[j].Name)
                                {
                                    myTask t = taskList[j];
                                    t.waitTime1++;
                                    taskList[j] = t;
                                    break;
                                }
                        }
                    }
                }
                runningList.Add(tmpTasks);

                runTime++;
            }

            tasksRinningList.Add(runningList);

            return getAverageWaitTime();
        }

        private Double ShortJobFirst()
        {
            List<myTask> runTaskList = new List<myTask>(this.taskList);
            List<List<myTask>> runningList = new List<List<myTask>>();

            bubbleSort(ref runTaskList, (task1, task2) =>
                task1.ExecutionTime.CompareTo(task2.ExecutionTime));

            int runTime = 0;
            while (runTaskList.Count > 0)
            {
                Boolean isFirst = true;
                List<myTask> tmpTasks = new List<myTask>();
                for (int i = 0; i < runTaskList.Count; i++)
                {
                    if (runTaskList[i].ExecutionTime == 0)
                    {
                        tmpTasks.Add(runTaskList[i]);
                        runTaskList.RemoveAt(i);
                        i--;
                    }
                    else if (isFirst)
                    {
                        tmpTasks.Add(runTaskList[i]);
                        myTask t = runTaskList[i];
                        t.ExecutionTime--;
                        runTaskList[i] = t;
                        isFirst = false;
                    }
                    else
                    {
                        for (int j = 0; j < taskList.Count; j++)
                            if (runTaskList[i].Name == taskList[j].Name)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                                break;
                            }
                    }
                }
                runningList.Add(tmpTasks);

                runTime++;
            }

            tasksRinningList.Add(runningList);

            return getAverageWaitTime();
        }

        private Double ShortJobFirst_Preemptive()
        {
            List<myTask> queueTaskList = new List<myTask>(this.taskList);
            bubbleSort(ref queueTaskList, (task1, task2) =>
               task1.ArrivalTime.CompareTo(task2.ArrivalTime));

            List<List<myTask>> runningList = new List<List<myTask>>();
            List<myTask> runTaskList = new List<myTask>();

            int runTime = 0;
            while (queueTaskList.Count > 0 || runTaskList.Count > 0)
            {
                for (int i = 0; i < queueTaskList.Count; i++)
                    if (queueTaskList[i].ArrivalTime <= runTime)
                    {
                        runTaskList.Add(queueTaskList[i]);
                        queueTaskList.RemoveAt(i);
                        i--;
                    }

                if (runTaskList.Count > 0 && runTaskList[0].ArrivalTime < runTime)
                {
                    myTask t = runTaskList[0];
                    t.ExecutionTime--;
                    runTaskList[0] = t;
                }

                for (int i = 1; i < runTaskList.Count; i++)
                    for (int j = 0; j < taskList.Count; j++)
                        if (runTaskList[i].Name == taskList[j].Name)
                        {
                            if (runTaskList[i].ArrivalTime < runTime)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                            }
                            break;
                        }

                runningList.Add(new List<myTask>(runTaskList));

                if (runTaskList.Count > 0 && runTaskList[0].ExecutionTime == 0)
                    runTaskList.RemoveAt(0);

                bubbleSort(ref runTaskList, (task1, task2) =>
                    task1.ExecutionTime.CompareTo(task2.ExecutionTime));

                runTime++;
            }

            tasksRinningList.Add(runningList);

            return getAverageWaitTime();
        }

        private Double ShortestRemainingTime()
        {
            List<myTask> runTaskList = new List<myTask>(this.taskList);
            List<List<myTask>> runningList = new List<List<myTask>>();

            bubbleSort(ref runTaskList, (task1, task2) =>
               task1.ArrivalTime.CompareTo(task2.ArrivalTime));

            int runTime = 0;
            while (runTaskList.Count > 0)
            {
                Boolean isFirst = true;
                List<myTask> tmpTasks = new List<myTask>();
                for (int i = 0; i < runTaskList.Count; i++)
                {
                    if (runTaskList[i].ArrivalTime <= runTime)
                    {
                        tmpTasks.Add(runTaskList[i]);
                        if (runTaskList[i].ExecutionTime == 0)
                        {
                            bubbleSort(ref runTaskList,
                                delegate (myTask task1, myTask task2)
                                {
                                    if (task1.ArrivalTime <= runTime && task2.ArrivalTime <= runTime)
                                        return task1.ExecutionTime.CompareTo(task2.ExecutionTime);
                                    else
                                        return -1;
                                });
                            runTaskList.RemoveAt(i);
                            i--;
                        }
                        else if (isFirst)
                        {
                            myTask t = runTaskList[i];
                            t.ExecutionTime--;
                            runTaskList[i] = t;
                            isFirst = false;
                        }
                        else
                        {
                            for (int j = 0; j < taskList.Count; j++)
                                if (runTaskList[i].Name == taskList[j].Name)
                                {
                                    myTask t = taskList[j];
                                    t.waitTime1++;
                                    taskList[j] = t;
                                    break;
                                }
                        }
                    }
                }
                runningList.Add(tmpTasks);

                runTime++;
            }

            tasksRinningList.Add(runningList);

            return getAverageWaitTime();
        }

        private Double HighPriorityFrist()
        {
            List<myTask> runTaskList = new List<myTask>(this.taskList);
            List<List<myTask>> runningList = new List<List<myTask>>();

            bubbleSort(ref runTaskList, (task1, task2) =>
               task1.ArrivalTime.CompareTo(task2.ArrivalTime));

            int runTime = 0;
            while (runTaskList.Count > 0)
            {
                Boolean isFirst = true;
                List<myTask> tmpTasks = new List<myTask>();
                for (int i = 0; i < runTaskList.Count; i++)
                {
                    if (runTaskList[i].ArrivalTime <= runTime)
                    {
                        tmpTasks.Add(runTaskList[i]);
                        if (runTaskList[i].ExecutionTime == 0)
                        {
                            runTaskList.RemoveAt(i);
                            i--;
                            bubbleSort(ref runTaskList,
                                delegate (myTask task1, myTask task2)
                                {
                                    if (task1.ArrivalTime <= runTime && task2.ArrivalTime <= runTime)
                                    {
                                        int comp = task1.Priority.CompareTo(task2.Priority);
                                        if (comp == 0)
                                            return task1.ArrivalTime.CompareTo(task2.ArrivalTime);
                                        else
                                            return comp;
                                    }
                                    else
                                        return -1;
                                });
                        }
                        else if (isFirst)
                        {
                            myTask t = runTaskList[i];
                            t.ExecutionTime--;
                            runTaskList[i] = t;
                            isFirst = false;
                        }
                        else
                        {
                            for (int j = 0; j < taskList.Count; j++)
                                if (runTaskList[i].Name == taskList[j].Name)
                                {
                                    myTask t = taskList[j];
                                    t.waitTime1++;
                                    taskList[j] = t;
                                    break;
                                }
                        }
                    }
                }
                runningList.Add(tmpTasks);

                runTime++;
            }

            tasksRinningList.Add(runningList);

            return getAverageWaitTime();
        }

        private Double RoundRobin(int Q = 5)
        {
            List<myTask> queueTaskList = new List<myTask>(this.taskList);
            bubbleSort(ref queueTaskList, (task1, task2) =>
               task1.ArrivalTime.CompareTo(task2.ArrivalTime));

            List<myTask> runTaskList = new List<myTask>();
            List<List<myTask>> runningList = new List<List<myTask>>();

            bool isFirst = true;
            int runTime = 0, Qtime = 0;
            while (queueTaskList.Count > 0 || runTaskList.Count > 0)
            {
                for (int i = 0; i < queueTaskList.Count; i++)
                    if (queueTaskList[i].ArrivalTime <= runTime)
                    {
                        runTaskList.Add(queueTaskList[i]);
                        queueTaskList.RemoveAt(i);
                        i--;
                    }

                if (runTaskList.Count > 0 && runTaskList[0].ArrivalTime < runTime)
                {
                    myTask t = runTaskList[0];
                    t.ExecutionTime--;
                    runTaskList[0] = t;

                    Qtime++;
                    isFirst = false;
                }

                for (int i = 1; i < runTaskList.Count; i++)
                    for (int j = 0; j < taskList.Count; j++)
                        if (runTaskList[i].Name == taskList[j].Name)
                        {
                            if (runTaskList[i].ArrivalTime < runTime)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                            }
                            break;
                        }

                runningList.Add(new List<myTask>(runTaskList));

                if (runTaskList.Count > 0 && runTaskList[0].ExecutionTime == 0)
                {
                    runTaskList.RemoveAt(0);
                    Qtime = 0;
                }
                else if (runTaskList.Count > 0 && Qtime % Q == 0 && !isFirst)
                {
                    runTaskList.Add(runTaskList[0]);
                    runTaskList.RemoveAt(0);

                    Qtime = 0;
                }

                runTime++;
            }

            tasksRinningList.Add(runningList);

            return getAverageWaitTime();
        }

        private Double MultipleLevelFeedbackQueue()
        {
            List<myTask> taskQueue = new List<myTask>(this.taskList);
            bubbleSort(ref taskQueue, (task1, task2) =>
               task1.ArrivalTime.CompareTo(task2.ArrivalTime));

            List<myTask> realtimePriorityQueue = new List<myTask>();
            List<myTask> highPriorityQueue = new List<myTask>();
            List<myTask> normalPriorityQueue = new List<myTask>();
            List<myTask> idlePriorityQueue = new List<myTask>();

            List<List<myTask>> runRealtimePriority = new List<List<myTask>>();
            List<List<myTask>> runHighPriority = new List<List<myTask>>();
            List<List<myTask>> runNormalPriority = new List<List<myTask>>();
            List<List<myTask>> runIdlePriority = new List<List<myTask>>();

            const int limitTime = 20;
            const int realtimeQSplit = 5, highQSplit = 6, normalQSplit = 7;
            int realtimeQ = 0, highQ = 0, normalQ = 0;
            Boolean isRealtimeLock = false, isHighLock = false, isNormalLock = false, isIdleLock = false;
            int runTime = 0;
            while (taskQueue.Count > 0 || realtimePriorityQueue.Count > 0 || highPriorityQueue.Count > 0 || normalPriorityQueue.Count > 0 || idlePriorityQueue.Count > 0)
            {
                for (int i = 0; i < taskQueue.Count; i++)
                    if (taskQueue[i].ArrivalTime <= runTime)
                    {
                        switch (taskQueue[i].Priority)
                        {
                            case 0:
                                realtimePriorityQueue.Add(taskQueue[i]);
                                break;
                            case 1:
                                highPriorityQueue.Add(taskQueue[i]);
                                break;
                            case 2:
                                normalPriorityQueue.Add(taskQueue[i]);
                                break;
                            default:
                                idlePriorityQueue.Add(taskQueue[i]);
                                break;
                        }

                        taskQueue.RemoveAt(i);
                        i--;
                    }


                if (realtimePriorityQueue.Count > 0 && (!isHighLock && !isNormalLock && !isIdleLock))
                {
                    if (realtimePriorityQueue[0].ArrivalTime < runTime)
                    {
                        myTask t = realtimePriorityQueue[0];
                        t.ExecutionTime--;
                        t.waitTime2 = 0;
                        t.runTime++;
                        realtimePriorityQueue[0] = t;

                        realtimeQ++;
                    }
                    isRealtimeLock = true;
                }
                else if (highPriorityQueue.Count > 0 && (!isRealtimeLock && !isNormalLock && !isIdleLock))
                {
                    if (highPriorityQueue[0].ArrivalTime < runTime)
                    {
                        myTask t = highPriorityQueue[0];
                        t.ExecutionTime--;
                        t.waitTime2 = 0;
                        t.runTime++;
                        highPriorityQueue[0] = t;

                        highQ++;
                    }
                    isHighLock = true;
                }
                else if (normalPriorityQueue.Count > 0 && (!isRealtimeLock && !isHighLock && !isIdleLock))
                {
                    if (normalPriorityQueue[0].ArrivalTime < runTime)
                    {
                        myTask t = normalPriorityQueue[0];
                        t.ExecutionTime--;
                        t.waitTime2 = 0;
                        t.runTime++;
                        normalPriorityQueue[0] = t;

                        normalQ++;
                    }
                    isNormalLock = true;
                }
                else if (idlePriorityQueue.Count > 0)
                {
                    if (idlePriorityQueue[0].ArrivalTime < runTime)
                    {
                        myTask t = idlePriorityQueue[0];
                        t.ExecutionTime--;
                        t.waitTime2 = 0;
                        t.runTime++;
                        idlePriorityQueue[0] = t;
                    }
                    isIdleLock = true;
                }

                int k;
                for (k = isRealtimeLock ? 1 : 0; k < realtimePriorityQueue.Count; k++)
                    for (int j = 0; j < taskList.Count; j++)
                        if (realtimePriorityQueue[k].Name == taskList[j].Name)
                        {
                            if (realtimePriorityQueue[k].ArrivalTime < runTime)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                            }
                            break;
                        }

                for (k = isHighLock ? 1 : 0; k < highPriorityQueue.Count; k++)
                    for (int j = 0; j < taskList.Count; j++)
                        if (highPriorityQueue[k].Name == taskList[j].Name)
                        {
                            if (highPriorityQueue[k].ArrivalTime < runTime)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                            }
                            break;
                        }

                for (k = isNormalLock ? 1 : 0; k < normalPriorityQueue.Count; k++)
                    for (int j = 0; j < taskList.Count; j++)
                        if (normalPriorityQueue[k].Name == taskList[j].Name)
                        {
                            if (normalPriorityQueue[k].ArrivalTime < runTime)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                            }
                            break;
                        }

                for (k = isIdleLock ? 1 : 0; k < idlePriorityQueue.Count; k++)
                    for (int j = 0; j < taskList.Count; j++)
                        if (idlePriorityQueue[k].Name == taskList[j].Name)
                        {
                            if (idlePriorityQueue[k].ArrivalTime < runTime)
                            {
                                myTask t = taskList[j];
                                t.waitTime1++;
                                taskList[j] = t;
                            }
                            break;
                        }

                runRealtimePriority.Add(new List<myTask>(realtimePriorityQueue));
                runHighPriority.Add(new List<myTask>(highPriorityQueue));
                runNormalPriority.Add(new List<myTask>(normalPriorityQueue));
                runIdlePriority.Add(new List<myTask>(idlePriorityQueue));

                if (realtimePriorityQueue.Count > 0 && realtimePriorityQueue[0].ExecutionTime == 0)
                {
                    realtimePriorityQueue.RemoveAt(0);
                    realtimeQ = 0;
                    isRealtimeLock = false;
                }
                else if (realtimePriorityQueue.Count > 0 && realtimeQ > 0 && realtimeQ % realtimeQSplit == 0)
                {
                    realtimePriorityQueue.Add(realtimePriorityQueue[0]);
                    realtimePriorityQueue.RemoveAt(0);

                    realtimeQ = 0;
                    isRealtimeLock = false;
                }

                if (highPriorityQueue.Count > 0 && highPriorityQueue[0].ExecutionTime == 0)
                {
                    highPriorityQueue.RemoveAt(0);
                    highQ = 0;
                    isHighLock = false;
                }
                else if (highPriorityQueue.Count > 0 && highQ > 0 && highQ % highQSplit == 0)
                {
                    highPriorityQueue.Add(highPriorityQueue[0]);
                    highPriorityQueue.RemoveAt(0);

                    highQ = 0;
                    isHighLock = false;
                }

                if (normalPriorityQueue.Count > 0 && normalPriorityQueue[0].ExecutionTime == 0)
                {
                    normalPriorityQueue.RemoveAt(0);
                    normalQ = 0;
                    isNormalLock = false;
                }
                else if (normalPriorityQueue.Count > 0 && normalQ > 0 && normalQ % normalQSplit == 0)
                {
                    normalPriorityQueue.Add(normalPriorityQueue[0]);
                    normalPriorityQueue.RemoveAt(0);

                    normalQ = 0;
                    isNormalLock = false;
                }

                if (idlePriorityQueue.Count > 0 && idlePriorityQueue[0].ExecutionTime == 0)
                {
                    idlePriorityQueue.RemoveAt(0);
                    isIdleLock = false;
                }

                for (int i = 0; i < realtimePriorityQueue.Count; i++)
                {
                    if (realtimePriorityQueue[i].runTime > limitTime && (!isRealtimeLock || i != 0))
                    {
                        myTask t1 = realtimePriorityQueue[i];
                        t1.runTime = 0;
                        t1.Priority++;
                        realtimePriorityQueue[i] = t1;

                        taskQueue.Add(realtimePriorityQueue[i]);
                        realtimePriorityQueue.RemoveAt(i);
                        i--;
                    }
                }

                for (int i = 0; i < highPriorityQueue.Count; i++)
                {
                    myTask t = highPriorityQueue[i];
                    t.waitTime2++;
                    highPriorityQueue[i] = t;

                    if (highPriorityQueue[i].waitTime2 > limitTime && (!isHighLock || i != 0))
                    {
                        myTask t1 = highPriorityQueue[i];
                        t1.waitTime2 = 0;
                        t1.Priority--;
                        highPriorityQueue[i] = t1;

                        taskQueue.Add(highPriorityQueue[i]);
                        highPriorityQueue.RemoveAt(i);
                        i--;
                    }
                    else if (highPriorityQueue[i].runTime > limitTime && (!isHighLock || i != 0))
                    {
                        myTask t1 = highPriorityQueue[i];
                        t1.runTime = 0;
                        t1.Priority++;
                        highPriorityQueue[i] = t1;

                        taskQueue.Add(highPriorityQueue[i]);
                        highPriorityQueue.RemoveAt(i);
                        i--;
                    }
                }

                for (int i = 0; i < normalPriorityQueue.Count; i++)
                {
                    myTask t = normalPriorityQueue[i];
                    t.waitTime2++;
                    normalPriorityQueue[i] = t;

                    if (normalPriorityQueue[i].waitTime2 > limitTime && (!isNormalLock || i != 0))
                    {
                        myTask t1 = normalPriorityQueue[i];
                        t1.waitTime2 = 0;
                        t1.Priority--;
                        normalPriorityQueue[i] = t1;

                        taskQueue.Add(normalPriorityQueue[i]);
                        normalPriorityQueue.RemoveAt(i);
                        i--;
                    }
                    else if (normalPriorityQueue[i].runTime > limitTime && (!isNormalLock || i != 0))
                    {
                        myTask t1 = normalPriorityQueue[i];
                        t1.runTime = 0;
                        t1.Priority++;
                        normalPriorityQueue[i] = t1;

                        taskQueue.Add(normalPriorityQueue[i]);
                        normalPriorityQueue.RemoveAt(i);
                        i--;
                    }
                }

                for (int i = 0; i < idlePriorityQueue.Count; i++)
                {
                    myTask t = idlePriorityQueue[i];
                    t.waitTime2++;
                    idlePriorityQueue[i] = t;

                    if (idlePriorityQueue[i].waitTime2 > limitTime && (!isIdleLock || i != 0))
                    {
                        myTask t1 = idlePriorityQueue[i];
                        t1.waitTime2 = 0;
                        t1.Priority--;
                        idlePriorityQueue[i] = t1;

                        taskQueue.Add(idlePriorityQueue[i]);
                        idlePriorityQueue.RemoveAt(i);
                        i--;
                    }
                }

                runTime++;
            }

            tasksRinningList.Add(runRealtimePriority);
            tasksRinningList.Add(runHighPriority);
            tasksRinningList.Add(runNormalPriority);
            tasksRinningList.Add(runIdlePriority);

            return getAverageWaitTime();
        }


        // 用滑鼠左鍵移動 panel
        Point preMousePoint, currentPoint;
        Boolean isMouseDown = false;
        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                ((PictureBox)sender).Cursor = Cursors.NoMove2D;
            isMouseDown = true;
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            ((PictureBox)sender).Cursor = Cursors.Default;
            isMouseDown = false;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            preMousePoint.X = currentPoint.X;
            preMousePoint.Y = currentPoint.Y;

            currentPoint.X = MousePosition.X;
            currentPoint.Y = MousePosition.Y;

            Panel panel = (Panel)((PictureBox)sender).Parent;
            if (panel.AutoScroll && e.Button == MouseButtons.Left && isMouseDown)
            {
                Point point = new Point();
                point.X = (Math.Abs(panel.AutoScrollPosition.X) - (currentPoint.X - preMousePoint.X));
                point.Y = (Math.Abs(panel.AutoScrollPosition.Y) - (currentPoint.Y - preMousePoint.Y));

                panel.AutoScrollPosition = point;
            }
        }

    }
}