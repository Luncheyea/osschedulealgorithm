作業系統的排程演算法

本練習具有：

    (1) 先來先服務 FCFS (First Come First Serve)

    (2) 最短工作先服務 SJF(Short Job First) (可搶先)

    (3) 剩餘最短的工作先服務 SRT(Shortest Remaining Time or Preemptive SJF)

    (4) 優先權 Priority

    (5) 巡迴服務 RR(Round Robin)(若Q=5)

    (6) 多層回饋佇列(Multilevel Feedback Queues)
